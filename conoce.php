<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--link rel="stylesheet" type="text/css" href="style.css" /-->
	<!--link rel="stylesheet" type="text/css" href="style.css" /-->
	<title>Sesi&oacute;n Magna de Estudios</title>
	<link href="css/vendor/bootstrap.min.css" rel="stylesheet">
	<link href="css/flat-ui.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link rel="shortcut icon" href="img/faviconmaes.ico">
</head>
    <body>
		
		<div>
	      <nav class="navbar navbar-inverse navbar-embossed" role="navigation">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
	            <span class="sr-only">Toggle navigation</span>
	          </button>
	          <a class="navbar-brand" href="./">Sesi&oacute;n Magna de Estudios</a>
	        </div>
	        <div class="collapse navbar-collapse" id="navbar-collapse-01">
	            <ul class="nav navbar-nav navbar-right">
	                <li><a href="./">Inscripci&oacute;n</a></li>
	                <li class="active"><a href="conoce.php">Informaci&oacute;n</a></li>
	                <li><a href="login.php">Iniciar Sesi&oacute;n</a></li>
	                <li><a href="http://asesoriasmaes.mty.itesm.mx/" target="_blank">Asesor&iacute;as<span class="navbar-unread">1</span></a></li>
	            </ul>
	        </div><!-- /.navbar-collapse -->
	      </nav><!-- /navbar -->
	    </div>

        <div class="container">
			  <div class="jumbotron">
		        <h1>¿Qu&eacute; es la Sesi&oacute;n Magna?</h1>
		        <p class="lead">La <b>Sesi&oacute;n Magna de Estudios</b> es la oportunidad de vivir una experiencia de s&uacute;per aprendizaje,
		        con base a la sugestopedia, que es el uso de los recursos de la mente para aprender y expandir la memoria.
		        Te invitamos a que cuando estudies, incluyas los pilares de la sugestopedia que son: respiraci&oacute;n, relajaci&oacute;n, m&uacute;sica,
		        programaci&oacute;n mental. Deseas obtener mejores resultados acad&eacute;micos… <b>¡Inscr&iacute;bete ya!</b></p>
		        <p>Realiza tu registro al evento anotando tus datos <a class="btn btn-lg btn-success" href="./" role="button" style="display:inline-block;padding-top:0;padding-bottom:0;">aqu&iacute;</a>, 
		        tendr&aacute;s oportunidad de dos bloques de asesor&iacute;as puedes elegir dos materias o una sola en ambos bloques.</p>
		      </div>

		      <div class="row marketing">
		        <div class="col-lg-6">
		          <h4>Participantes</h4>

					<?php
                        include ("conexion.php"); 
                        $maxaux = mysql_query("SELECT maximo FROM maximoalumnos");
						$totalaux = mysql_query("SELECT COUNT(*) FROM alumnos;");
						$max = mysql_result($maxaux, 0);
						$total = mysql_result($totalaux, 0);
						echo '<p><b>'. $total . '</b> alumnos registrados. <b>'. $max .'</b> como máximo.</p>';
                    ?>
		        </div>

		        <div class="col-lg-6">
		          <h4>M&aacute;s informaci&oacute;n</h4>
		          <span class="fui-facebook"><a href="https://fb.com/maes.itesm" target="_blank"> maes.itesm</a></span>
		        </div>
		      </div>
		</div>

    	<script src="js/vendor/jquery.min.js"></script>
    	<script src="js/flat-ui.min.js"></script>

    </body>
</html>
