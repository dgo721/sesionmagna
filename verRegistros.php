<?php

include "accesa.php";

if (isset($_POST['borrar'])){
	$delete = $_POST["borrar"];
		include("conexion.php");
		$borra = mysql_query("DELETE FROM alumnos WHERE matricula = '$delete'");
		if (!$borra) { echo '<script type="text/javascript"> alert("Error.") </script>'; }
		else{ echo '<script type="text/javascript"> alert("Alumno Borrado.") </script>';}
}

if (isset($_POST['max'])){
	$max = $_POST["max"];
		include("conexion.php");
		$borra = mysql_query("UPDATE maximoAlumnos SET maximo = '$max'");
		if (!$borra) { echo '<script type="text/javascript"> alert("Error.") </script>'; }
		else{ echo '<script type="text/javascript"> alert("Numero maximo actualizado.") </script>';}
}
?>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Sesi&oacute;n Magna de Estudios</title>
    <link href="css/vendor/bootstrap.min.css" rel="stylesheet">
    <link href="css/flat-ui.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/faviconmaes.ico">
</head>
    <body>

        <div>
          <nav class="navbar navbar-inverse navbar-embossed" role="navigation">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
                <span class="sr-only">Toggle navigation</span>
              </button>
              <a class="navbar-brand" href="verRegistros.php">Sesi&oacute;n Magna de Estudios</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse-01">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="verRegistros.php">Alumnos</a></li>
                    <li><a href="verMaterias.php">Primera Hora</a></li>
                    <li><a href="verMaterias2.php">Segunda Hora</a></li>
                    <li><a href="verAdministrador.php">Admins</a></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
          </nav><!-- /navbar -->
        </div>

        <div class="container">
            <div align="center">
                <h3>Alumnos Registrados</h3>
                <?php
                    include("conexion.php");
                    $results = mysql_query("SELECT * FROM maximoalumnos");
                    while ($row = mysql_fetch_array($results)) {
                ?>
                <?php echo '
                        <form action="verRegistros.php" method="post"> 
                        <label>M&aacute;ximo:&nbsp;</label>
                        <input id="max" name ="max" type="text" value="'.$row['maximo'].'" style="text-align:center;">
                        <input type="submit" value="Guardar" text-align="center"/> </form>
                        '
                ?>
                <?php
                    }
                ?>
            </div>
            <table class="table table-striped" width="100%">
                <thead>
                    <tr>
                        <th>Matricula</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Materia 1</th>
                        <th>Materia 2</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    include("conexion.php");
                    $result = mysql_query("SELECT * FROM alumnos ORDER BY fecha DESC");

                    if($result === FALSE) { 
                        echo '<h5>No existen alumnos registrados</h5>';
                        //die(mysql_error());
                    } else {
                        while ($row = mysql_fetch_array($result)) {
                    ?>
                            <tr>
                                <td><?php echo $row['matricula'] ?></td>
                                <td><?php echo $row['nombre'] ?></td>
                                <td><?php echo $row['correo'] ?></td>
                                <td><?php echo $row['materia1'] ?></td>
                                <td><?php echo $row['materia2'] ?></td>
                                <td style="background-color:#f4aeae;"><?php echo '<form action="verRegistros.php" method="post">
                                <input type="hidden" name="borrar" value="'.$row['matricula'].'" />
                                <input type="submit" value="Borrar" style="background-color:#e49595;" /> </form>' ?></td>
                            </tr>

                            <?php
                        }
                    }

                    ?>
                </tbody>
            </table>

            <div class="row" align="center"><a href="aexcel.php" align="center" class="btn btn-block btn-lg btn-primary" style="width:20%;">Descargar Base en Excel</a></div>
        </div>

        <script src="js/vendor/jquery.min.js"></script>
        <script src="js/flat-ui.min.js"></script>
    </body>
</html>
