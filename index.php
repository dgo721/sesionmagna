<?php

include ("conexion.php");

function removerAcento($cadena){
    $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
    $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
    $cadena = utf8_decode($cadena);
    $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
    return utf8_encode($cadena);
}

if (isset($_POST['matricula']) and isset($_POST['nombre']) and isset($_POST['correo']) and isset($_POST['materia1']) and isset($_POST['materia2'])) {
    
        $maxaux = mysql_query("SELECT maximo FROM maximoalumnos");
        $totalaux = mysql_query("SELECT COUNT(*) FROM alumnos;");
        $max = mysql_result($maxaux, 0);
        $total = mysql_result($totalaux, 0);

        date_default_timezone_set('America/Monterrey');
        $date = new DateTime();
        $fecha = date_format($date, 'Y-m-d H:i:s');

        if($total < $max){
            $matricula = $_POST['matricula'];
            $nombre = removerAcento($_POST['nombre']);
            $correo = $_POST['correo'];
            
            $materia1 = removerAcento($_POST['materia1']);
            $materia2 = removerAcento($_POST['materia2']);
            
            $ingresa = mysql_query("INSERT INTO Alumnos VALUES ('$matricula', '$nombre', '$correo', '$materia1', '$materia2', '$fecha')");
            //$ingresa = mysql_query("INSERT INTO Alumnos VALUES ('$matricula', '$nombre', '$correo', '$materia1', 'NULL', '$fecha')");
            if (!$ingresa) { echo '<script type="text/javascript"> alert("Alumno ya está registrado.") </script>'; }
            else { echo '<script type="text/javascript"> alert("Registro exitoso.") </script>';}
        }
        else{
            echo '<script type="text/javascript"> alert("Ya se llegó al total de alumnos, no te puedes inscribir.") </script>';
        }
    
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--link rel="stylesheet" type="text/css" href="style.css" /-->
<title>Sesi&oacute;n Magna de Estudios</title>
<link href="css/vendor/bootstrap.min.css" rel="stylesheet">
<link href="css/flat-ui.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="shortcut icon" href="img/faviconmaes.ico">

<script type="text/javascript">
            var prendida1 = false;
            var prendida2 = false;
            
            function otra1(name){
                if(name=='Otra')
                    document.getElementById('otra1').innerHTML='<input type="text" value="" name="materia1" id="otramateria1" placeholder="Materia 1" class="form-control" />';
                else
                    document.getElementById('otra1').innerHTML='';
            }
               
            function otra2(name){
                if(name=='Otra')
                    document.getElementById('otra2').innerHTML='<input type="text" value="" name="materia2" id="otramateria2" placeholder="Materia 2" class="form-control" />';
                else
                    document.getElementById('otra2').innerHTML='';
            }
            
            function validar(){
                document.getElementById("notaMatricula").innerHTML="";
                document.getElementById("notaNombre").innerHTML="";
                document.getElementById("notaCorreo").innerHTML="";
                document.getElementById("notamateria1").innerHTML="";
                document.getElementById("notamateria2").innerHTML="";
                
                var valido;
                
                var matricula= form1.matricula.value;
                
                if(matricula =="" || !(matricula.substring(0,2)=="A0")){
                    valido = false;
                    document.getElementById("notaMatricula").innerHTML="<span class=\"demo-text-note\">Ingresa una matr&iacute;cula</span>";
                }
                
                var nombre= form1.nombre.value;
                
                 if(nombre ==""){
                    valido = false;
                    document.getElementById("notaNombre").innerHTML="<span class=\"demo-text-note\">Ingresa un nombre</span>";
                }
                
                var correo= form1.correo.value;

                if(correo=="")
                 {
                    valido = false;
                    document.getElementById("notaCorreo").innerHTML="<span class=\"demo-text-note\">Ingresa un correo</span>";
                }

                var materia1= form1.materia1.value;
                if(materia1=="-Selecciona materia-"){
                	valido = false;
                	document.getElementById("notamateria1").innerHTML="<span class=\"demo-text-note\">Selecciona una materia</span>";
                }

                var materia2= form1.materia2.value;
                if(materia2=="-Selecciona materia-"){
                	valido = false;
                	document.getElementById("notamateria2").innerHTML="<span class=\"demo-text-note\">Selecciona una materia</span>";
                }

                return valido;
                    
            }
        </script>
</head>

<body>
    <div>
      <nav class="navbar navbar-inverse navbar-embossed" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
            <span class="sr-only">Toggle navigation</span>
          </button>
          <a class="navbar-brand" href="./">Sesi&oacute;n Magna de Estudios</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-01">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="./">Inscripci&oacute;n</a></li>
                <li><a href="conoce.php">Informaci&oacute;n</a></li>
                <li><a href="login.php">Iniciar Sesi&oacute;n</a></li>
                <li><a href="http://asesoriasmaes.mty.itesm.mx/" target="_blank">Asesor&iacute;as<span class="navbar-unread">1</span></a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
      </nav><!-- /navbar -->
    </div>

    <figure style="float:left;margin-top:-20px;margin-left:20px;">
        <img  src="img/logo-tecnologico.jpg" alt="Tecnologico de Monterrey">
    </figure>

    <figure>
        <img id="logoMAESmedia" src="img/maes.jpg" alt="Tecnologico de Monterrey">
    </figure>

    <div class="container">

        <!--h1 align="center">Sesi&oacute;n Magna de Estudios</h1-->
        <br /><br />

       <div class="row">
            <div class="col-xs-5">
                <div>
                	<img src="img/maes.jpg" id="logoMAES" alt="Tecnologico de Monterrey" align="right">
                </div>
                <div align="center">
                	<a href="conoce.php" id="btn-conoce" class="btn btn-block btn-lg btn-info">Conoce m&aacute;s</a>
                </div>
            </div> <!-- /sliders -->

            <div class="col-xs-6 col-xs-offset-1">
                <h3 class="demo-panel-title">Sesi&oacute;n Magna de Estudios</h3>
                <div class="demo-type-example">
                    <p>Ingresa tus datos. La matricula debe empezar con "A0".</p>
                </div>

                <div>
                    <form action="#" method="post" name="form1" id="form1" onsubmit="return validar()">
                    <div class="row">
                    <div class="col-xs-3">
                    	<label>Matr&iacute;cula:</label>
                    </div>
                    <div class="col-xs-9">
                    	<div class="form-group">
                        	<input type="text" value="" name ="matricula" id="matricula" placeholder="A0*******" class="form-control"/>
                        	<div id="notaMatricula" style="color: darkred;"></div>
                        </div>
	                </div>
	                </div>
	                <div class="row">
	                <div class="col-xs-3">
                    	<label>Nombre:</label>
                    </div>
                    <div class="col-xs-9">
                    	<div class="form-group">
                        	<input type="text" value="" name="nombre" id="nombre" placeholder="Nombre" class="form-control" />
                        	<div id="notaNombre" style="color: darkred;"></div>
                        </div>
  
	                </div>
	                </div>
	                <div class="row">
	                <div class="col-xs-3">
                    	<label>Correo:</label>
                    </div>
                    <div class="col-xs-9">
                    	<div class="form-group">
                        	<input type="text" value="" name="correo" id="correo" placeholder="ejemplo@outlook.com" class="form-control" />
                        	<div id="notaCorreo" style="color: darkred;"></div>
                        </div>
                    </div>
                    </div>
                    <div class="row">
	                <div class="col-xs-3">
                    	<label>Materia 1:</label>
                    </div>
                    <div class="col-xs-9">
                    	<div class="form-group">
                    		<select name="materia1" onchange="otra1(this.value)" id="materia1" class="form-control select select-primary" data-toggle="select">
	                            <option>-Selecciona materia-</option>

	                            <?php
	                                include("conexion.php");
	                                $sql = "SELECT * FROM  `materias` ORDER BY  `materia` ASC";
                                    $result = mysql_query($sql) or die (mysql_error());
	                                while($row = mysql_fetch_array($result)) {
	                                    $materia = $row['materia'];
	                                    echo '<option>'.$materia.'</option>';
	                                }
	                            ?>

	                            <option>Otra</option>                          
	                        </select>
	                        <div id="otra1" name="materia1" style="color: darkred;"></div>
	                        <div id="notamateria1" style="color: darkred;"></div>
                    	</div>
	                </div>
	                </div>
	                <div class="row">
	                <div class="col-xs-3">
                    	<label>Materia 2:</label>
                    </div>
                    <div class="col-xs-9">
                    	<div class="form-group">
                    		<select name="materia2" onchange="otra2(this.value)" id="materia2" class="form-control select select-primary" data-toggle="select" disabled="disabled">
	                            <option>-Selecciona materia-</option>

	                            <?php
	                                include("conexion.php");
	                                $sql = "SELECT * FROM  `materias2` ORDER BY  `materia` ASC";
	                                $result = mysql_query($sql) or die (mysql_error());
	                                while($row = mysql_fetch_array($result)) {
	                                    $materia = $row['materia'];
	                                    echo '<option>'.$materia.'</option>';
	                                }
	                            ?>

	                            <option>Otra</option>
	                        </select>
	                        <div id="otra2" name="materia2" style="color: darkred;"></div>
	                        <div id="notamateria2" style="color: darkred;"></div>
                    	</div>
	                </div>
	                </div>

                    <div class="col-xs-12" align="center">
                        <input type="submit" value="Enviar" />
                    </div>
                </form>
                </div>
              
            </div> <!-- /navigation -->
      </div> <!-- /row -->
        
    </div>            

    <script src="js/vendor/jquery.min.js"></script>
    <script src="js/flat-ui.min.js"></script>
        
</body>
</html>
