<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--link rel="stylesheet" type="text/css" href="style.css" /-->
	<!--link rel="stylesheet" type="text/css" href="style.css" /-->
	<title>Sesi&oacute;n Magna de Estudios</title>
	<link href="css/vendor/bootstrap.min.css" rel="stylesheet">
	<link href="css/flat-ui.min.css" rel="stylesheet">
	<link rel="shortcut icon" href="img/faviconmaes.ico">
</head>
    <body>
		
		<div>
	      <nav class="navbar navbar-inverse navbar-embossed" role="navigation">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
	            <span class="sr-only">Toggle navigation</span>
	          </button>
	          <a class="navbar-brand" href="./">Sesi&oacute;n Magna de Estudios</a>
	        </div>
	        <div class="collapse navbar-collapse" id="navbar-collapse-01">
	            <ul class="nav navbar-nav navbar-right">
	                <li><a href="./">Inscripci&oacute;n</a></li>
	                <li><a href="conoce.php">Informaci&oacute;n</a></li>
	                <li class="active"><a href="login.php">Iniciar Sesi&oacute;n</a></li>
	                <li><a href="http://asesoriasmaes.mty.itesm.mx/" target="_blank">Asesor&iacute;as<span class="navbar-unread">1</span></a></li>
	            </ul>
	        </div><!-- /.navbar-collapse -->
	      </nav><!-- /navbar -->
	    </div>

        <div class="container">
	        <div class="login-screen" style="padding-left:300px;padding-right:300px;background-color:#1abc9c">
	          <div class="login-icon">
	            <img src="img/icons/png/Compas.png" alt="Iniciar Sesión" />
	            <h4 style="color:#021110;"><small>Sesi&oacute;n Magna de Estudios</small>Iniciar Sesi&oacute;n</h4>
	          </div>

	          <div class="login-form">

	          <form action="loginFunc.php" method="post" enctype="multipart/form-data" name="form1" id="form1">
                    <div class="form-group">
		              <input type="text" class="form-control login-field" value="" placeholder="Usuario" name="usuario" id="usuario"/>
		              <label class="login-field-icon fui-user" for="login-name"></label>
		            </div>

		            <div class="form-group">
		              <input type="password" class="form-control login-field" value="" placeholder="Contraseña" name="password" id="password" />
		              <label class="login-field-icon fui-lock" for="login-pass"></label>
		            </div>

		            <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
              </form>
              		<a class="login-link" href="./">Inscr&iacute;bete</a>
	          </div>
	        </div>
        </div>

        <?php
			if (isset($_GET['error'])){

				if ($_GET['error'] == 1) {
		?>
					<script>alert("Usuario o contraseña incorrectos")</script>
		<?php
				}

				else if ($_GET['error'] == 2){
		?>
					<script>alert("Se requiere inicio de sesión.")</script>
		<?php

				}
			}
		?>

    	<script src="js/vendor/jquery.min.js"></script>
    	<script src="js/flat-ui.min.js"></script>

    </body>
</html>